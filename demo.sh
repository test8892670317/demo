#!/bin/bash

CHOIX=$(dialog --title "Question" --menu "Es-tu Bilel ?" 15 60 4 \
"1" "Oui" \
"2" "Non" \
"3" "Sans commentaire" \
3>&1 1>&2 2>&3)

case $CHOIX in
  1)
    echo "Vous avez choisi: Oui"
    ;;
  2)
    echo "Vous avez choisi: Non"
    ;;
  3)
    echo "Vous avez choisi: Sans commentaire"
    ;;
  *)
    echo "Option invalide"
    ;;
esac
